#!/usr/bin/env node
"use strict";
import { t, TextToJSON, TextToTerminal } from "@mimer/text";
import { mainModule } from "process";
import yargs from "yargs";
import {aview} from "./aview";

async function main() {
let result = t();

const args = yargs.command("about", "Information about tihs program.")
.command("start", "start the server");
const argv = args.argv;
const command = argv._[0];

if(await aview(argv)) {

switch (command) {
  case "about":
    result.add("This is a game under construction!").green;
    break;
  case "start":
    result.add("This should start a server").green;
    break;
  default:
    result.add("Bad option").red;
    break;
}


if (false) {
  console.log(TextToJSON(result));
}

if (true) {
  console.log(TextToTerminal(result));
}

}
}

main();