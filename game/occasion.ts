import { Calculation, c } from "@mimer/calculation";
import { t, Text } from "@mimer/text";
import Creature from "../world/creatures/creature";
import { BattelAction } from "./battelAction";
import { Effect } from "./effect";

/**
 * Represents that some things i occuring in the battel.
 */
export class Occasion<T = {}> {
  action: BattelAction<T>;
  target?: Creature;
  accuracy: Calculation;
  effect: Effect<T>[] = [];
  type: "enemy" | "self" = "enemy";
  quantity: number = 1;
  data?: T;

  constructor(action: BattelAction<T>, data: T | undefined = undefined) {
    this.action = action;
    this.accuracy = c();
    this.data = data;
  }

  /**
   * Create an effect on this Occasion
   */
  createEffect(): Effect<T> {
    const result = new Effect<T>(this);
    this.effect.push(result);
    return result;
  }

  /**
   * Make any dice roll for this Occasion.
   */
  roll() {
    this.accuracy.roll();
  }

  /**
   * Make a short text describing the damage.
   *
   * @return a text describing the damage.
   */
  get showDamageLabel(): Text {
    return t(
      this.target!.labelName,
      " blir ",
      this.action.verb,
      " av ",
      this.action.by.labelName,
      ": "
    ).span(64);
  }

  /**
   * Return a text of the title.
   *
   * @return a text of the title.
   */
  get showTitel(): Text {
    return this.action.showName.headline;
  }
}
