"use strict";
import { Text } from "@mimer/text";
import Player from "./player";

export type gameStatus = "start" |"next"|"end"|"error";

export interface Config {
  name?: string;
}

/**
 * The  Game class creting a game engine for logic.
 */

export default abstract class Game {
  name: string;
  players: Player[];
  status: gameStatus = "start";

  constructor(players: Player[], config:Config={}) {
    this.name = config.name ?? "";
    this.players = players;
  }

  abstract next(id: number, answer: { [key: string]: number | string }): gameStatus;
}
